import os
import re

from googlesearch import search

for file in os.listdir("Python"):
    file.replace(" ", "-")
    print(file)

    with open('Python/' + file, 'r+') as f:

        if re.search("http", f.readline()):
            continue
        print("\tNot Found")

        for url in search(file + ' challenge site:hackerrank.com/challenges', domains=['hackerrank.com'], tld='com',
                          stop=1):
            url = '# ' + re.sub(r'/forum/?.*', '', url)
            print("\t" + url)

        file_data = f.read()
        f.seek(0)
        f.write(url + "\n\n" + file_data)

