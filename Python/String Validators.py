s1 = input()
print(
    True if True in [True if e else False for e in s1 if e.isalnum()] else False,
    True if True in [True if e else False for e in s1 if e.isalpha()] else False,
    True if True in [True if e else False for e in s1 if e.isdigit()] else False,
    True if True in [True if e else False for e in s1 if e.islower()] else False,
    True if True in [True if e else False for e in s1 if e.isupper()] else False,
    sep="\n"
)