import numpy as np

print(round(np.linalg.det(np.array([list(map(float, input().split())) for n in range(int(input()))])), 2))
