# https://www.hackerrank.com/challenges/find-second-maximum-number-in-a-list

a = [[input(), float(input())] for x in range(int(input()))]
print("\n".join(sorted([x[0] for x in a if x[1] == sorted(set(x[1] for x in a))[1]])))
