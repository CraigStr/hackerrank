# https://www.hackerrank.com/challenges/validating-postalcode

import re

regex_integer_in_range = "^\d{6}$"
regex_alternating_repetitive_digit_pair = "^([0-9a-zA-Z])(?!.*\1$)[0-9a-zA-Z]{5,}$"

P = input()

print(bool(re.match(regex_integer_in_range, P))
      and len(re.findall(regex_alternating_repetitive_digit_pair, P)) < 2)