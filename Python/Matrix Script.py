from re import sub

length, size = map(int, input().split())
ar = [[] for x in range(size)]

for x in range(length):
    z = input()
    for y in range(size):
        ar[y].append(z[y])

a = "".join([str(x) for i in ar for x in i])
print(a)

b = sub(r"(?<=\w)(\W+)(?=\w)", " ", a)
print(b)