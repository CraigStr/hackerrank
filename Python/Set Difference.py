# https://www.hackerrank.com/challenges/py-set-difference-operation

e1, e2 = input(), set(input().split(" "))
f1, f2 = input(), set(input().split(" "))
print(len(e2.difference(f2)))
