from itertools import combinations
input()

x = list(combinations(input().split(" "), int(input())))
print(sum([1 for y in x if "a" in y]) / len(x))
