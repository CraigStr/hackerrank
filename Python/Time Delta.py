# https://www.hackerrank.com/challenges/python-time-delta

from datetime import datetime as dt

print(*[int(abs((dt.strptime(input(), "%a %d %b %Y %H:%M:%S %z") -
                                   dt.strptime(input(), "%a %d %b %Y %H:%M:%S %z")).total_seconds()))
                          for x in range(int(input()))], sep="\n")
