# https://www.hackerrank.com/challenges/triangle-quest-2

for i in range(1, int(input()) + 1):
    # print(*[abs(abs(x - i + 1) - i) for x in range(i * 2 - 1)], sep="")
    f = lambda x: abs(abs(x - i + 1) - i, i)
    print(f)
