# https://www.hackerrank.com/challenges/merge-the-tools

# l = list(input())
# a = [l[i:i + n] for i in range(0, len(l), n)]
# print("\n".join(["".join(sorted(set(a[i]), key=a[i].index)) for i in range(int(input()))]))


def merge_the_tools(l, n):
    a = [list(l)[i:i + n] for i in range(0, len(l), n)]
    for x in a:
        z = []
        for y in x:
            z.append(y) if y not in z else ""
        print("".join(z))


if __name__ == '__main__':
    string, k = input(), int(input())
    merge_the_tools(string, k)
