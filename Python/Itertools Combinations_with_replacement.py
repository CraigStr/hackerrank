# https://www.hackerrank.com/challenges/itertools-combinations-with-replacement

from itertools import combinations_with_replacement

s, n = input().split(" ")
print(*["".join(x) for x in combinations_with_replacement(sorted(s), int(n))], sep="\n")
