# https://www.hackerrank.com/challenges/itertools-permutations/problem

from itertools import permutations

s, n = input().split(" ")
print(*["".join(x) for x in permutations(sorted(s), int(n))], sep="\n")
