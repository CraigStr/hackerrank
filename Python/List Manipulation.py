n = []
for x in range(int(input())):
    y = input().split(" ")
    if y[0] == "insert":
        n.insert(int(y[1]), int(y[2]))
    elif y[0] == "remove":
        n.remove(int(y[1]))
    elif y[0] == "append":
        n.append(int(y[1]))
    elif y[0] == "pop":
        n.pop()
    elif y[0] == "reverse":
        n.reverse()
    elif y[0] == "sort":
        n = sorted(n)
    elif y[0] == "print":
        print(n)
