from html.parser import HTMLParser


class MyHTMLParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        print("Start :", tag)
        for x in attrs:
            print("->", x[0], ">", x[1])

    def handle_endtag(self, tag):
        print("End   :", tag)

    def handle_startendtag(self, tag, attrs):
        print("Empty :", tag)
        for x in attrs:
            print("->", x[0], ">", x[1])


parser = MyHTMLParser()
parser.feed(''.join([input().strip() for x in range(int(input()))]))
