student_marks = {}
for _ in range(int(input())):
    name, *line = input().split()
    scores = list(map(float, line))
    student_marks[name] = scores
print("{:.2f}".format(sum(student_marks.get(input()))/3))
