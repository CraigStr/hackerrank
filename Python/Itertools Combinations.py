from itertools import combinations

s, n = input().split(" ")
print(*["".join(y) for x in range(1, int(n)+1) for y in combinations(sorted(s), int(x))], sep="\n")
