input()
A = set(input().split(" "))
for x in range(int(input())):
    i1 = input().split()
    i2 = set(input().split(" "))
    if i1[0] == "intersection_update":
        A.intersection_update(i2)
    elif i1[0] == "update":
        A.update(i2)
    elif i1[0] == "symmetric_difference_update":
        A.symmetric_difference_update(i2)
    elif i1[0] == "difference_update":
        A.difference_update(i2)
print(sum(map(int, A)))