# https://www.hackerrank.com/challenges/most-commons

s = list(input())
x = sorted([(y, s.count(y)) for y in set(s)], key=lambda x: (-x[1], x[0]))[:3:]
print("\n".join([y[0] + " " + str(y[1]) for y in x]))
