import re

pattern = re.compile(
    r"^"
    r"(?!.*(\d)(-?\1){3})"
    r"[456]"
    r"\d{3}"
    r"(?:-?\d{4}){3}"
    r"$")

cc_list = [
    # valid
    "4253625879615786",
    "4424424424442444",
    "5122-2368-7954-3214",

    # invalid
    "42536258796157867",
    "4424444424442444",
    "5122-2368-7954 - 3214",
    "44244x4424442444",
    "0525362587961578",

]

for num in cc_list:
    print(re.fullmatch(pattern, num))