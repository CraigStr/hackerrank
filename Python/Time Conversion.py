# https://www.hackerrank.com/challenges/time-conversion

# from datetime import datetime as dt

def timeConversion(s):
    a = s.split(":")
    a[0] = str((int(a[0]) % 12) + 12) if "PM" in a[2] else str(int(a[0]) % 12).zfill(2)
    a[2] = a[2][:2:]
    return ":".join(a)


print(timeConversion("12:05:45AM"))
