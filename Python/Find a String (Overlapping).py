def count_substring(s1, s2):
    count = 0
    index = 0
    while True:
        index = s1.find(s2, index)
        if index >= 0:
            count += 1
            index += 1
        else:
            break
    return count


count_substring("ABCDCDC", "CDC")
