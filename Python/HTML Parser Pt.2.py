from html.parser import HTMLParser


class HtmlParser(HTMLParser):
    def handle_comment(self, data):
        if '\n' in data or '\r\n' in data:
            print('>>> Multi-line Comment')
            print("", data)
        else:
            print('>>> Single-line Comment')
            print("", data)

    def handle_data(self, data):
        if data != '\n':
            print(">>> Data")
            print(data)


html_parser = HtmlParser()
html_parser.feed('\n'.join([input() for _ in range(int(input()))]))
html_parser.close()
