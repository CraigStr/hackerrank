# https://www.hackerrank.com/challenges/py-the-captains-room

a, x = input(), input().split()
for b in x:
    if x.count(b) > 1:
        x = [y for y in x if y != b]
print(x[0])
