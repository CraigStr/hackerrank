x, y = map(int, input().split(" "))
for x in range(1, x // 2 + 1):
    print('{:-^{z}}'.format('.|.' * (2 * x - 1), z=y))
print('{:-^{z}}'.format("WELCOME", z=y))
for x in range(x, 0, -1):
    print('{:-^{z}}'.format('.|.' * (2 * x - 1), z=y))
