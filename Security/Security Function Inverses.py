input()
print(*[y[1] for y in sorted([[x, index + 1] for index, x in enumerate(input().split())], key=lambda x: x[0])], sep="\n")
