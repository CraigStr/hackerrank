# https://www.hackerrank.com/challenges/birthday-cake-candles

def birthdayCakeCandles(ar):
    return ar.count(max(ar))

print(birthdayCakeCandles([3, 2, 1, 3]))