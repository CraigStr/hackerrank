# https://www.hackerrank.com/challenges/apple-and-orange

def countApplesAndOranges(s, t, a, b, apples, oranges):
    print(sum([s <= l + a <= t for l in apples]),
          sum([s <= l + b <= t for l in oranges]), sep="\n")


countApplesAndOranges(7, 11, 5, 15, [-2, 2, 2, 1], [5, -5, -3, -4, -5, -6])
