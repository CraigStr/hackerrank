# https://www.hackerrank.com/challenges/grading

import math


def gradingStudents(grades):
    return [int(5 * math.ceil(float(x) / 5)) if (x % 10 in [3, 4, 8, 9] and x > 38) else x for x in grades]


print(gradingStudents([73, 67, 38, 33]))
