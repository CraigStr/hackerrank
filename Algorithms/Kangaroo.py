# https://www.hackerrank.com/challenges/kangaroo

def kangaroo(x1, v1, x2, v2):
    if v2 == v1: return "Yes"
    return "YES" if (v2 < v1) and ((x2 - x1) % (v2 - v1)) == 0 else "NO"


print(kangaroo(43, 2, 70, 2))