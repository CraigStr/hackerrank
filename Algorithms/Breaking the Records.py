# https://www.hackerrank.com/challenges/breaking-best-and-worst-records


def breakingRecords(scores):
    cl = ch = scores[0]
    lcount = hcount = 0
    for x in scores:
        if x < cl:
            cl = x
            lcount += 1
        elif ch < x:
            ch = x
            hcount += 1
    return [hcount, lcount]


print(*breakingRecords([3, 4, 21, 36, 10, 28, 35, 5, 24, 42]))
