# https://www.hackerrank.com/challenges/staircase

def staircase(n):
    print(*[str("#"*x).rjust(n, " ") for x in range(1, n+1)], sep="\n")


staircase(5)