# https://www.hackerrank.com/challenges/beautiful-triplets

def compareTriplets(a, b):
    z = [x + -y for x, y in zip(a, b)]
    return [sum([1 for x in z if x > 0]), sum([1 for x in z if x < 0])]

print(compareTriplets([17, 28, 30], [99, 16, 8]))
