# https://www.hackerrank.com/challenges/magic-square-forming

magic_squares = [[[8, 1, 6], [3, 5, 7], [4, 9, 2]],
                 [[6, 1, 8], [7, 5, 3], [2, 9, 4]],
                 [[4, 9, 2], [3, 5, 7], [8, 1, 6]],  # !!
                 [[2, 9, 4], [7, 5, 3], [6, 1, 8]],
                 [[8, 3, 4], [1, 5, 9], [6, 7, 2]],
                 [[4, 3, 8], [9, 5, 1], [2, 7, 6]],
                 [[6, 7, 2], [1, 5, 9], [8, 3, 4]],
                 [[2, 7, 6], [9, 5, 1], [4, 3, 8]]]


def formingMagicSquare(matrix):
    # Since there are only 8 possibilities for 3x3 magic squares, we can use these as a sort of lookup table
    # https://mindyourdecisions.com/blog/2015/11/08/how-many-3x3-magic-squares-are-there-sunday-puzzle/

    cost_list = []
    for magic_square in magic_squares:
        cost = 0
        for indexx, x in enumerate(matrix):
            for indexy, y in enumerate(x):
                if y != magic_square[indexx][indexy]:
                    cost += abs(y - magic_square[indexx][indexy])
        if cost == 0:  # if already a perfect square, no need to compute further
            return 0
        cost_list.append(cost)
    return min(cost_list)


print(formingMagicSquare(
    [[4, 8, 2],
     [4, 5, 7],
     [6, 1, 6]]
))
