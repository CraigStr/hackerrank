# https://www.hackerrank.com › challenges › bon-appetit


def bonAppetit(bill, k, b):
    anna_total = sum(bill[:k:] + bill[k + 1::]) // 2
    return "Bon Appetit" if b == anna_total else b - anna_total


print(bonAppetit([3, 10, 2, 9], 1, 12))
