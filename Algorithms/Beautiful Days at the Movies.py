# https://www.hackerrank.com/challenges/beautiful-days-at-the-movies

def beautifulDays(i, j, k):
    # for x in range(i, j + 1):
    #     if (x - int(str(x)[::-1])) % k == 0:
    #         print(True)
    return sum([True if (x - int(str(x)[::-1])) % k == 0 else False for x in range(i, j + 1)])


print(beautifulDays(20, 23, 6))
