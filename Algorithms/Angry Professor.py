# https://www.hackerrank.com/challenges/angry-professor

def angryProfessor(k, a):
    return "NO" if len([x for x in a if x <= 0]) >= k else "YES"


print(angryProfessor(2, [0, -1, 2, 1]))
