# https://www.hackerrank.com/challenges/counting-valleys

def countingValleys(n, s):
    curr_height = 0
    count = 0
    for x in s:
        curr_height += 1 if x == 'U' else -1
        if curr_height == 0 and x == 'U':
            count += 1
    return count


heights = "UDDDUDUU"
print(countingValleys(len(heights), heights))
