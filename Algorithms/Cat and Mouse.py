# https://www.hackerrank.com/challenges/cats-and-a-mouse

def catAndMouse(x, y, z):
    return "Mouse C" if abs(x - z) == abs(y - z) else "Cat A" if abs(x - z) < abs(y - z) else "Cat B"


print(catAndMouse(1, 2, 3))
