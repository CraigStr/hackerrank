# https://www.hackerrank.com/challenges/mini-max-sum

def miniMaxSum(arr):
    print(sum(sorted(arr)[:4]), sum(sorted(arr)[:0:-1]))

miniMaxSum([1, 3, 5, 7, 9])