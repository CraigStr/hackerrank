# https://www.hackerrank.com/challenges/encryption/problem

import math


def encryption(s):
    len_ar = math.ceil(math.sqrt(len(s)))
    ar = []
    while s:
        ar.append(list(s[:len_ar:]))
        ar[-1] += [''] * (len_ar - len(ar[-1]))
        s = s[len_ar::]

    result = ''
    for x in range(len(ar[0])):
        temp = ''
        for y in range(len(ar)):
            temp += ar[y][x]
        result += temp + ' '
    return result[:-1:]


print(encryption('feedthedog'))
