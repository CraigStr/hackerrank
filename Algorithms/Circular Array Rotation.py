# https://www.hackerrank.com/challenges/circular-array-rotation/problem


def circularArrayRotation(a, k, queries):
    len_a = len(a)
    rot = a[-k % len_a::] + a[:-k % len_a:]

    checks = []

    for i in queries:
        checks.append(rot[i])
    return checks

print(circularArrayRotation(["a", "b", "c", "d"], 2, [1]))
