# https://www.hackerrank.com/challenges/drawing-book/problem


def pageCount(n: int, p: int):
    if n / 2 >= p:
        return p // 2
    else:
        return (n - p + (n + 1) % 2) // 2


print(pageCount(5, 5))
