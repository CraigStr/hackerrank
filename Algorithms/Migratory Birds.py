# https://www.hackerrank.com/challenges/migratory-birds

def migratoryBirds(arr):
    return max(set(arr), key=arr.count)


print(migratoryBirds([1, 1, 2, 2, 3]))
