# https://www.hackerrank.com/challenges/save-the-prisoner/problem


def saveThePrisoner(n: int, m: int, s: int):
    poisoned = (m + s - 1) % n
    return poisoned if poisoned else n


print(saveThePrisoner(7, 19, 2))
print(saveThePrisoner(3, 7, 3))
