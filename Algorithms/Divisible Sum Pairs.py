# https://www.hackerrank.com/challenges/divisible-sum-pairs

from itertools import combinations

def divisibleSumPairs(n, b, a):
    return sum(1 for x in list(combinations(a, 2)) if sum(x) % b == 0)


print(divisibleSumPairs(6, 3, [1, 3, 2, 6, 1, 2]))