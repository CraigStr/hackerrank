# https://www.hackerrank.com/challenges/plus-minus

def plusMinus(arr):
    print(*[sum(1 for x in arr if x > 0) / len(arr), sum(1 for x in arr if x < 0) / len(arr),
            sum(1 for x in arr if x == 0) / len(arr)], sep="\n")


plusMinus([-4, 3, -9, 0, 4, 1])
