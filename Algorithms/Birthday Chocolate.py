# https://www.hackerrank.com/challenges/the-birthday-bar

def birthday(s, d, m):
    return sum(True for x in range(len(s)) if d == sum(s[x: x + m]))


print(birthday([4], 4, 1))
