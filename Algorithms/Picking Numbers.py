# https://www.hackerrank.com/challenges/picking-numbers

def pickingNumbers(a: list) -> int:
    return max([a.count(i) + a.count(i + 1) for i in a])


print(pickingNumbers([1, 2, 2, 3, 4]))
