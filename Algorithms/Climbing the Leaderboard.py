# https://www.hackerrank.com/challenges/climbing-the-leaderboard


def climbingLeaderboard(scores, alice):
    scores = sorted(set(scores), reverse=True)
    ans = []
    num_scores = len(scores)

    for al_score in alice:
        while num_scores and al_score >= scores[num_scores - 1]:
            num_scores -= 1
        ans.append(num_scores + 1)
    return ans


print(climbingLeaderboard([100, 100, 50, 40, 40, 20, 10],
                          [5, 25, 50, 120]))
print(climbingLeaderboard([100, 90, 90, 80, 75, 60],
                          [50, 65, 77, 90, 102]))
