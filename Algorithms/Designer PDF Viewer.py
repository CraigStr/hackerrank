# https://www.hackerrank.com/challenges/designer-pdf-viewer/problem


def designerPdfViewer(h, word):
    maxh = max([h[ord(x) - 97] for x in word])
    return maxh * len(word)


print(designerPdfViewer([1, 3, 1, 3, 1, 4, 1, 3, 2, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 7], 'zaba'))
