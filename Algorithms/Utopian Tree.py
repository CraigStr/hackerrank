# https://www.hackerrank.com/challenges/utopian-tree


def utopianTree(n):
    """The Utopian Tree goes through 2 cycles of growth every year. Each spring, it doubles in height. Each summer, its height increases by 1 meter.
       Laura plants a Utopian Tree sapling with a height of 1 meter at the onset of spring. How tall will her tree be after
       growth cycles?"""

    # height = 1
    # for x in range(n):
    #     if x % 2 == 1:
    #         height += 1
    #     else:
    #         height *= 2
    # return height

    # return [height += 1 for x in range(n) if x % 2 == 1 else height *= 2]

    return ~(~1 << (n >> 1)) << n % 2


print(utopianTree(5))
