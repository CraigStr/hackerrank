# https://www.hackerrank.com/challenges/strange-advertising/problem


def viralAdvertising(n: int):
    shares = 5
    likes = 0
    for _ in range(0, n):
        likes += shares >> 1
        shares = (shares >> 1) * 3
    return likes


print(viralAdvertising(5))
