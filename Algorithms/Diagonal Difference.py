# https://www.hackerrank.com/challenges/diagonal-difference

def diagonalDifference(arr):
    return abs(sum([arr[x][y] for x in range(len(arr)) for y in range(len(arr)) if x == y]) - sum(
        arr[i][len(arr) - i - 1] for i in range(len(arr))))


print(diagonalDifference([[11, 2, 4], [4, 5, 6], [10, 8, -12]]))
