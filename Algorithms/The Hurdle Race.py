# https://www.hackerrank.com/challenges/the-hurdle-race/problem


def hurdleRace(k, height):
    num = max(height) - k
    return num if num > 0 else 0


print(hurdleRace(7, (2, 5, 4, 5, 2)))
