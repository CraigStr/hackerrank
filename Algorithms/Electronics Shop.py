# https://www.hackerrank.com/challenges/electronics-shop

from itertools import product


def getMoneySpent(keyboards, drives, b):
    prices = product(keyboards, drives)
    return max(k + d if k + d <= b else -1 for k, d in prices)


print(getMoneySpent([3, 1],
                    [5, 2, 8],
                    10))
