# https://www.hackerrank.com/challenges/sock-merchant

def sockMerchant(n, ar):
    return sum([ar.count(i) // 2 for i in set(ar)])


print(sockMerchant(10, [1, 1, 3, 1, 2, 1, 3, 3, 3, 3]))
